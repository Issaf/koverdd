export {
  scrollingOptions
}

function scrollingOptions(offset) {
  return {
    duration: 700,
    offset: offset,
    easing: "easeInOutCubic",
  }
}
