import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import "./main.scss"
import en from '../translations/english.ts'
import fr from '../translations/french.ts'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'md',
  },  
  lang: {
    locales: { en, fr },
    current: 'en'
  }
});