export default {
  dataIterator: {
    rowsPerPageText: 'Items per page:',
    rowsPerPageAll: 'All',
    pageText: '{0}-{1} of {2}',
    noResultsText: 'No matching records found',
    nextPage: 'Next page',
    prevPage: 'Previous page'
  },
  dataTable: {
    rowsPerPageText: 'Rows per page:'
  },
  noDataText: 'No data available',
  carousel: {
    prev: 'Previous visual',
    next: 'Next visual'
  },

  aboveTheFoldComponent: {
    headline: "Grow your business without worrying about all the technical hassle.",
    ctaBtn: "Book a free consultation"
  },
  whatWeDoComponent: {
    title: "What we do",
    section1Title: "In-depth audit of your business",
    section1Description: "We will analyze your business in details from understanding your market and unique offer, to your needs and goals.",
    section2Title: "Website design & building",
    section2Description: "Using the findings from the audit, we will design, build, and set up your website including all the marketing tools. From simple online front, bookings manager, online payments, or e-commerce store, We've got you koverdd.",
    section3Title: "Marketing & customer targeting",
    section3Description: "Using your new online setup, we will focus on result-driven marketing campaigns to identify, segment and ultimately get more clients.",
    section4Title: "Analytics & Insights",
    section4Description: "We provide several analytics services and reports so you can make data-driven decision on what to focus on, what customers are more likely to buy, when, and how.",
    whyImportantTitle: "Why is this important for you?",
    whyImportantParagraph1: "Nowadays, Having an online presence for a business of any size is vital. Whether it's a product or a service, customers want find it easily, and they want it fast. But for most business owners, all the technical stuff can be a real nightmare. Although some resources are available online, you end up spending hours learning and trying to do it yourself instead of focusing on serving your customers.",
    whyImportantParagraph2: "This is where we can really help you. We will design, build, and analyse your online presence with a very clear outcome in mind: making it as easy as possible for your right customers to find all the relevant information and take action then finding as many of those customers as possible."
  },
  aboutUsComponent: {
    title: "About us",
    paragraph: "We are a small team of passionate programmers, designers, and marketers based in Montreal. Our mission is to give small businesses access to an entire external technology department. Through beautiful and intuitive design, and a ROI-centric approach, we help our client delight their customers and grow their business."
  },
  ourWorkComponent: {
    title: "Our work",
    project1: {
      title: "Project",
      paragraph1: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae libero pellentesque, convallis nibh id, suscipit felis. Nunc vitae fermentum metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec imperdiet dui erat, at tempus lectus imperdiet eget. Vivamus pharetra ex blandit massa imperdiet interdum. Nullam vitae lectus vel orci venenatis consectetur. Mauris ut ligula sit amet ligula rhoncus mollis at nec sem. Sed viverra elit risus, non scelerisque sem auctor non. Pellentesque ipsum ex, rutrum luctus tellus vitae, finibus luctus lectus. Morbi blandit, tortor sit amet sollicitudin viverra, nisi augue pellentesque erat, ut consectetur odio urna eleifend enim. Proin tincidunt fringilla est ac accumsan. Maecenas semper posuere libero, ac fringilla nisl finibus quis. Integer scelerisque dapibus risus ut tincidunt.",
      paragraph2: "Sed eu auctor orci, ac mollis est. In a euismod tortor, non porttitor enim. Maecenas vestibulum metus augue, ut eleifend nibh luctus ut. Aenean dapibus consectetur magna sed lacinia. Cras gravida nibh non tempor scelerisque. Nunc eu accumsan dolor. Donec ac urna mauris. Vivamus pretium tortor nulla, ac congue orci congue non. Nam pulvinar id dolor ac bibendum.",
      paragraph3: "Sed interdum et nunc non scelerisque. Nam tempor ipsum sit amet erat porta semper. Sed diam turpis, hendrerit ut risus in, fermentum venenatis libero. Vivamus a cursus metus, et mollis metus. Proin laoreet et orci in mattis. Duis convallis, est quis blandit volutpat, quam eros dignissim magna, id ultricies lacus elit sed sapien. Aliquam eget eleifend dolor, at condimentum enim. Vestibulum convallis tempus urna, quis facilisis enim lacinia sodales. Nulla nibh ipsum, efficitur fringilla blandit quis, sagittis nec eros. Aenean et mi nec ex consectetur cursus.",
    }
  },
  footerComponent: {
    part1: "Copyrights",
    part2: ". All rights reserved." 
  },
  contactComponent: {
    title: "Contact",
    fillerPreEmail: "We’d love to hear from you. Email us at ",
    fillerPostEmail: " or just use the form below. We will get back to you within 24h.",
    firstNamePlaceholder: "Firstname",
    emailPlaceholder: "Email",
    msgBodyPlaceholder: "Hello Koverdd team!",
    requiredValidation: "Required field.",
    emailValidation: "Invalid Email.",
    btnLabel: "Send",
    snackbarMsgSent: "Message sent!",
    snackbarMsgError: "Oops...Please try again"
  },
  navbarComponent: {
    whatWeDoLabel: "What we do",
    aboutUsLabel: "About Us",
    ourWorkLabel: "Our work",
    contactLabel: "Contact",
    editRdvLabel: "Cancel appointment",
    cta: "Book free call",
    langBtnText: "Français"
  },
  schedulerComponent: {
    add: {
      title: "Let's connect!",
      subtitle: "Free Consultation",
      description: "We will have a 15 min call (by phone, Skype, or Zoom) to discuss your business, needs, and goals. No commitment or payment infos required.",
      pickDate: "Pick a date",
      pickTime: "Pick a time",
      pickDateFirst: "Pick a date first",
      nextBtnLabel: "next",
      pickCommMedium: "How can we call you?",
      phoneMedium: "By phone",
      skypeMedium: "By Skype",
      zoomMedium: "By Zoom",
      clientInfoFormHeader: "Alight one last step!",
      descriptionPlaceholder: "Please tell us a little bit about your business and industry",
      phonePlaceholder: "Phone Number",
      otherMediumPlaceholder: "Username",
      finalBtnLabel: "Book"
    },
    edit: {
      subtitle: "Cancel Appointment",
      description: "Just enter your email below to cancel your appointment.",
      cancelBtn: "Cancel Appointment"
    },
    errorMsgs: {
      general: "Something went wrong... Please try again.",
      existingEmail: "This email exists already.",
      emailNotFound: "Email not found."
      
    }
  },
  donePage: {
    sentence1: "Your free consultation has been successfully ",
    sentence2: ". You should receive a confirmation email shortly.",
    booked: "booked",
    cancelled: "cancelled"
  }
}
