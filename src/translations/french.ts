export default {
  dataIterator: {
    rowsPerPageText: 'Élements par page:',
    rowsPerPageAll: 'Tous',
    pageText: '{0}-{1} de {2}',
    noResultsText: 'Aucun enregistrement correspondant trouvé',
    nextPage: 'Page suivante',
    prevPage: 'Page précédente'
  },
  dataTable: {
    rowsPerPageText: 'Lignes par page:'
  },
  noDataText: 'Aucune donnée disponible',
  carousel: {
    prev: 'Visuel précédent',
    next: 'Visuel suivant'
  },

  aboveTheFoldComponent: {
    headline: "La puissance du web n'est pas accessible que pour les grosses compagnies",
    ctaBtn: "RÉSERVEZ UN APPEL GRATUIT"
  },
  whatWeDoComponent: {
    title: "Nos Services",
    section1Title: "Audit de votre business",
    section1Description: "Nous analyserons votre business en détails pour bien comprendre votre marché, ce qui vous rend unique, votre offre, vos besoins et objectifs.",
    section2Title: "Construction de votre site web",
    section2Description: "Nous ferons le design et développement de votre site web, incluant tous les outils marketing. Que ce soit une simple vitrine en ligne, un gestionnaire de réservation, payements en ligne, ou un site e-commerce, on est là.",
    section3Title: "Marketing & ciblage de clients",
    section3Description: "Une fois que tout est en place, nous nous concentrerons sur des compagnes marketing précises et mesurables pour identifier, segmenter, et avoir plus de clients.",
    section4Title: "Données & Intelligence",
    section4Description: "Nous offrons aussi une gamme de rapports et services analytiques pour vous permettre de prendre de meilleures décisions sur quels clients cibler, quand le faire, de quelles façons.",
    whyImportantTitle: "Pourquoi c'est important?",
    whyImportantParagraph1: "De nos jours, avoir une présence en ligne pour une compagnie est vital, peu importe sa taille. Qu’il s’agisse d’un produit ou d’un service, les clients veulent le trouver facilement, et ils le veulent rapidement. Pour la majorité des gérants cependant, l’aspect technique peut être un vrai cauchemar. Vous finissez par passer des heures à tout apprendre et essayer de le faire vous-même au lieu de vous focaliser sur mieux servir vos clients.",
    whyImportantParagraph2: "C’est là qu’on peut vraiment vous aider. Nous ferons le design, développement et optimisation de votre présence en ligne avec un but bien précis: rendre le processus pour vos clients de trouver les informations nécessaires et agir (réservations, achat, inscription, ...) aussi facile que possible. Ensuite, trouver de plus en plus de ces clients."
  },
  aboutUsComponent: {
    title: "À propos",
    paragraph: "Nous sommes une petite équipe basée à Montréal de programmeurs, designers, et marketeurs. Notre mission est de donner aux petites entreprises accès à un département technique complet. À travers des designs beaux et intuitifs et une obsession pour le retour sur investissement, nous aidons nos clients à ravir leurs clients et croitre leurs entreprises."
  },
  ourWorkComponent: {
    title: "Nos Clients",
    project1: {
      title: "Projet",
      paragraph1: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae libero pellentesque, convallis nibh id, suscipit felis. Nunc vitae fermentum metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec imperdiet dui erat, at tempus lectus imperdiet eget. Vivamus pharetra ex blandit massa imperdiet interdum. Nullam vitae lectus vel orci venenatis consectetur. Mauris ut ligula sit amet ligula rhoncus mollis at nec sem. Sed viverra elit risus, non scelerisque sem auctor non. Pellentesque ipsum ex, rutrum luctus tellus vitae, finibus luctus lectus. Morbi blandit, tortor sit amet sollicitudin viverra, nisi augue pellentesque erat, ut consectetur odio urna eleifend enim. Proin tincidunt fringilla est ac accumsan. Maecenas semper posuere libero, ac fringilla nisl finibus quis. Integer scelerisque dapibus risus ut tincidunt.",
      paragraph2: "Sed eu auctor orci, ac mollis est. In a euismod tortor, non porttitor enim. Maecenas vestibulum metus augue, ut eleifend nibh luctus ut. Aenean dapibus consectetur magna sed lacinia. Cras gravida nibh non tempor scelerisque. Nunc eu accumsan dolor. Donec ac urna mauris. Vivamus pretium tortor nulla, ac congue orci congue non. Nam pulvinar id dolor ac bibendum.",
      paragraph3: "Sed interdum et nunc non scelerisque. Nam tempor ipsum sit amet erat porta semper. Sed diam turpis, hendrerit ut risus in, fermentum venenatis libero. Vivamus a cursus metus, et mollis metus. Proin laoreet et orci in mattis. Duis convallis, est quis blandit volutpat, quam eros dignissim magna, id ultricies lacus elit sed sapien. Aliquam eget eleifend dolor, at condimentum enim. Vestibulum convallis tempus urna, quis facilisis enim lacinia sodales. Nulla nibh ipsum, efficitur fringilla blandit quis, sagittis nec eros. Aenean et mi nec ex consectetur cursus.",
    }
  },
  footerComponent: {
    part1: "Koverdd Inc.",
    part2: ". Tous droits réservés." 
  },
  contactComponent: {
    title: "Nous Joindre",
    fillerPreEmail: "Si vous avez des questions, n’hésitez pas à nous joindre par email à ",
    fillerPostEmail: " ou tout simplement en utilisant le formulaire ci-dessous. Vous aurez une réponse dans les 24 h.",
    firstNamePlaceholder: "Prénom",
    emailPlaceholder: "Email",
    msgBodyPlaceholder: "Salut team Koverdd!",
    requiredValidation: "Champs requis.",
    emailValidation: "Email non-valide.",
    btnLabel: "Envoyer",
    snackbarMsgSent: "Message envoyé!",
    snackbarMsgError: "Oops...Veuillez reéssayer"
  },
  navbarComponent: {
    whatWeDoLabel: "Nos Services",
    aboutUsLabel: "À propos",
    ourWorkLabel: "Nos Clients",
    contactLabel: "Nous Joindre",
    editRdvLabel: "Annuler Réservation",
    cta: "Appel Gratuit",
    langBtnText: "English"
  },
  schedulerComponent: {
    add: {
      title: "Discutons!",
      subtitle: "Consultation Gratuite",
      description: "Nous aurons un appel de 15 min (par téléphone, Skype ou Zoom) pour discuter de votre business, besoins, et objectifs. 100% sans engagement.",
      pickDate: "Choisissez une date",
      pickTime: "Choisissez une heure",
      pickDateFirst: "Veuillez choisir une date d'abord",
      nextBtnLabel: "suivant",
      pickCommMedium: "Moyen de communication",
      phoneMedium: "Par phone",
      skypeMedium: "Par Skype",
      zoomMedium: "Par Zoom",
      clientInfoFormHeader: "Dernière étape!",
      descriptionPlaceholder: "Dites nous un peu plus sur votre business et industrie",
      phonePlaceholder: "Numéro de téléphone",
      otherMediumPlaceholder: "Nom d'utilisateur",
      finalBtnLabel: "réserver"
    },
    edit: {
      subtitle: "Annuler Réservation",
      description: "Veuillez entrer votre email ci-dessous pour annuler votre réservation.",
      cancelBtn: "Annuler Réservation"
    },
    errorMsgs: {
      general: "Oops...Veuillez reéssayer",
      existingEmail: "Cet email est déjà utilisé",
      emailNotFound: "Cet email n'a pas été trouvé"
      
    }
  },
  donePage: {
    sentence1: "Votre consultation gratuite a bien été ",
    sentence2: ". Vous devriez recevoir un email de confirmation sous peu.",
    booked: "réservée",
    cancelled: "annulée"
  }

}
