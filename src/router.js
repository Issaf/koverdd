import Vue from 'vue'
import Router from 'vue-router'
import MainPage from './views/Main.page'
import SchedulerHome from './views/SchedulerHome.page'
import ContactPage from './views/Contact.page'
import DonePage from './views/Done.page'

Vue.use(Router)

export default new Router({
  mode: "history",
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/",
      component: MainPage
    },
    {
      path: "/contact",
      component: ContactPage
    },
    {
      path: "/consultation",
      component: SchedulerHome,
      alias: ["/consultation/cancel"]
    },
    {
      path: "/done",
      component: DonePage
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
})
